//
//  AuthManager.swift
//  Instagram-demo
//
//  Created by Abraam on 04.02.2022.
//

import FirebaseAuth
public class AuthManager {
    static let shared = AuthManager()
    // MARK: - Public
    public func registerNewUser(username:String,email:String,password:String,completion:@escaping(Bool)->Void){
        /*
         - Check if username is available
         - Check if email is available
        
         */
        DatabaseManager.shared.canCreateNewUser(with: email, username: username){canCreate in
            if canCreate{
            /*
             - Create Account
             - Insert Account to database
             */
                Auth.auth().createUser(withEmail: email, password: password) { result, error in
                    guard error == nil,result !=  nil else {
                     // Firebase auth could not create account
                        completion(false)
                        return
                    }
                    // insert into database
                    DatabaseManager.shared.insertNewUser(with: email, username: username){inserted in
                        if inserted {
                            completion(true)
                            return
                        }else {
                            //Failed to insert to database
                            completion(false)
                            return
                        }
                        
                    }
                }
            }else {
                // username or email Does not exit
                completion(false)
            }
            
        }
    }
    public func loginUser(username:String?,email:String?,password:String,completion:@escaping(Bool) -> Void){
        if let email = email {
            // Email Login
            Auth.auth().signIn(withEmail: email, password: password) { authResult, error in
                guard authResult != nil , error == nil else {
                   completion(false)
                    return
                }
                completion(true)
            }
        } else if let username = username {
            //Username Login
            print(username)
        }
        
    }
    /// Attemt to logOut Firebase user
    public func logOut(completion:(Bool)->Void){
        do {
            
            try Auth.auth().signOut()
            completion(true)
            return
        }catch{
            print(error)
            completion(false)
            return
            
        }
    }
    
}
