//
//  ViewController.swift
//  Instagram-demo
//
//  Created by Abraam on 03.02.2022.
//

import UIKit
import FirebaseAuth
class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
      handleNotAuthed()
//        do{
//            try FirebaseAuth.Auth.auth().signOut()
//        }catch{
//            print("error")
//        }
        
        
    }
    private func handleNotAuthed(){
        // Check Auth Status
        if Auth.auth().currentUser == nil {
            let loginVC = LoginViewController()
            loginVC.modalPresentationStyle = .fullScreen
            present(loginVC, animated: false)
            
        }
    }




}
