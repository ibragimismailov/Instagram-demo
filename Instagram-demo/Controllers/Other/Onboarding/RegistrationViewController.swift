//
//  RegistrationViewController.swift
//  Instagram-demo
//
//  Created by Abraam on 03.02.2022.
//

import UIKit

class RegistrationViewController: UIViewController {
    struct Constants {
        static let cornerRadius:CGFloat = 8.0
    }
    private let usernameField:UITextField = {
        let field = UITextField()
        field.placeholder = "Username ..."
        field.returnKeyType = .next
        field.leftViewMode = .always
        field.leftView = UIView(frame: CGRect(x: 0,
                                              y: 0,
                                              width: 10,
                                              height: 0))
        field.autocapitalizationType = .none
        field.layer.masksToBounds = true
        field.backgroundColor = .secondarySystemBackground
        field.layer.cornerRadius = Constants.cornerRadius
        field.layer.borderWidth = 0.3
        field.layer.borderColor = UIColor.secondaryLabel.cgColor
        return field
    }()
    private let emailField:UITextField = {
        let field = UITextField()
        field.placeholder = "Email adress..."
        field.returnKeyType = .next
        field.leftViewMode = .always
        field.leftView = UIView(frame: CGRect(x: 0,
                                              y: 0,
                                              width: 10,
                                              height: 0))
        field.autocapitalizationType = .none
        field.layer.masksToBounds = true
        field.backgroundColor = .secondarySystemBackground
        field.layer.cornerRadius = Constants.cornerRadius
        field.layer.borderWidth = 0.3
        field.layer.borderColor = UIColor.secondaryLabel.cgColor
        return field
    }()

    
    private let passwordFiel:UITextField = {
        let field = UITextField()
        field.placeholder = "Password..."
        field.returnKeyType = .continue
        field.leftViewMode = .always
        field.leftView = UIView(frame: CGRect(x: 0,
                                              y: 0,
                                              width: 10,
                                              height: 0))
        field.autocapitalizationType = .none
        field.layer.masksToBounds = true
        field.layer.cornerRadius = Constants.cornerRadius
        field.backgroundColor = .secondarySystemBackground
        field.layer.borderWidth = 0.3
        field.layer.borderColor = UIColor.secondaryLabel.cgColor
        field.isSecureTextEntry = true
        return field
    }()
    
    private let registerButton:UIButton = {
        let button = UIButton()
        button.setTitle("Sign Up", for: .normal)
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(didtapRegiser), for: .touchUpInside)
        button.layer.cornerRadius = Constants.cornerRadius
        button.backgroundColor = .systemGreen
        button.setTitleColor(.white, for: .normal)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        usernameField.delegate = self
        emailField.delegate = self
        passwordFiel.delegate = self
        view.backgroundColor = .systemBackground
        view.addSubview(usernameField)
        view.addSubview(emailField)
        view.addSubview(passwordFiel)
        view.addSubview(registerButton)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        usernameField.frame = CGRect(x: 20, y: view.safeAreaInsets.top+10, width: view.width-40, height: 52)
        emailField.frame = CGRect(x: 20, y: usernameField.bottom+10, width: view.width-40, height: 52)
        passwordFiel.frame = CGRect(x: 20, y: emailField.bottom+10, width: view.width-40, height: 52)
        registerButton.frame = CGRect(x: 20, y: passwordFiel.bottom+10, width: view.width-40, height: 52)
    }
    @objc private func didtapRegiser(){
        passwordFiel.resignFirstResponder()
        emailField.resignFirstResponder()
        usernameField.resignFirstResponder()
        guard let email = emailField.text ,      !email.isEmpty,
              let password = passwordFiel.text  ,!password.isEmpty,password.count >= 8,
              let username = usernameField.text ,!username.isEmpty else {
                  return
              }
        AuthManager.shared.registerNewUser(username: username, email: email, password: password){registered in
            DispatchQueue.main.async {
                if registered {
                    //good
                }else{
                    //failed
                }
            }
        }
    }
   
}
extension RegistrationViewController:UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == usernameField {
            emailField.becomeFirstResponder()
        }else if textField == emailField {
            passwordFiel.becomeFirstResponder()
        }else {
            didtapRegiser()
        }
        return true
    }
}
